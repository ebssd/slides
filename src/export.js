// external dependencies
// linux with tmpfs under /tmp, inkscape, pdftk
// sudo apt install inkscape pdftk
// for presentation `impressive` is recommended

const fs = require("fs");
const path = require("path");
const { exec, execSync } = require("child_process");
const cheerio = require("cheerio");
const _ = require("lodash");
const { sprintf } = require("sprintf-js");

const tmpPath = "/tmp/slideExport";

const exportPdf = true;

if (fs.existsSync(tmpPath)) {
  fs.readdirSync(tmpPath).forEach(f => {
    fs.unlinkSync(path.join(tmpPath, f));
  });
  fs.rmdirSync(tmpPath);
}

fs.mkdirSync(tmpPath);

function promiseFromChildProcess(child) {
  return new Promise(function(resolve, reject) {
    child.addListener("error", reject);
    child.addListener("exit", resolve);
  });
}

function hideAll(elements) {
  elements.forEach(e => {
    e.attribs["style"] = "display:none";
  });
}

function show(element) {
  // console.log(element.attribs);
  element.attribs["style"] = "display:inline";
  // console.log(element.attribs);
}

var contents = fs.readFileSync("./slides.svg", "utf8");
const svg = cheerio.load(contents, {
  xmlMode: true
});

const layers = {};
svg("g[inkscape\\:groupmode=layer]").each((idx, element) => {
  layers[element.attribs["inkscape:label"]] = element;
});

// console.log(_.keys(layers));
const masters = _.chain(layers)
  .keys()
  .filter((v, idx) => {
    if (v.match(/^m[0-9]+$/g) !== null) {
      return true;
    } else {
      return false;
    }
  })
  .value();

const pages = _.chain(layers)
  .keys()
  .map((key, idx) => {
    const m = key.match(/^p-(m[0-9]+)-(.+)$/);

    if (m !== null && m.length === 3) {
      return {
        id: m[0],
        master: m[1],
        chapter: m[2],
        slide: layers[key]
      };
    } else {
      return null;
    }
  })
  .filter(v => v !== null)
  .value();

let counter = 0;

// console.log(pages);
const procs = [];
pages.forEach(p => {
  hideAll(_.values(layers));
  show(layers[p.master]);
  show(layers[p.id]);
  const name = sprintf("p%04d", counter);
  console.log("id: " + p.id);
  console.log("name is: " + name);
  const file = path.join(tmpPath, `${name}.svg`);
  fs.writeFileSync(
    file,
    svg
      .xml()
      .replace(/{{chapter}}/g, p.chapter)
      .replace(/{{page}}/g, `${counter + 1} / ${pages.length}`)
  );

  if (exportPdf) {
    const child = exec(
      `inkscape -A ${tmpPath}/${name}.pdf ${tmpPath}/${name}.svg`
    );
    procs.push(promiseFromChildProcess(child));
  }

  counter++;
});

if (exportPdf) {
  Promise.all(procs).then(() => {
    execSync(`pdftk ${tmpPath}/*.pdf output slides.pdf`);
  });
}

// console.log(_.pick(layers, masters));
// console.log(_.keys(layers));
